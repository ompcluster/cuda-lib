#include <omp.h>
#include <stdio.h>
#include "cuda-lib.h"

#pragma omp declare target(caller)

#define N 2

int main() {
	int values[N];
	for (int i = 0; i < N; i++)
		values[i] = N * i + 10;

	for (int i = 0; i < N; i++)
		printf("HEADNODE %d\n", values[i]);

#pragma omp target map(to: values[:N])
	{
		for (int i = 0; i < N; i++)
			printf("WORKER CPU %d\n", values[i]);
		caller(values);
	}

	return 0;
}
