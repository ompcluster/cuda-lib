#ifndef CUDA_LIB_H
#define CUDA_LIB_H

#ifdef __cplusplus
extern "C" {
#endif

void caller(int values[2]);

#ifdef __cplusplus
}
#endif

#endif // CUDA_LIB_H
