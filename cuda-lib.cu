#include <cuda.h>
#include <stdio.h>
#include "cuda-lib.h"
#include "cuda_error.cuh"

__global__ void kernel(int* values, int c) {
  int x = threadIdx.x + blockIdx.x * blockDim.x;
	printf("WORKER - GPU %d-%d\n", x, values[x]);
}

void caller(int values[2]) {
  printf("WORKER - Function from cuda lib\n");

  int* d_values;

  cudaMalloc((void **)&d_values, 2 * sizeof(int));
  cudaMemcpy(d_values, values, 2 * sizeof(int), cudaMemcpyHostToDevice);

	kernel<<<1, 2>>>(d_values, 4);
  CUDA_CHECK_LAST();

  cudaDeviceSynchronize();
}
